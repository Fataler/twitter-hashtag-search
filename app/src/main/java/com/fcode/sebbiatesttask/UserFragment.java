package com.fcode.sebbiatesttask;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Ruslan on 06.11.2015.
 */
public class UserFragment extends Fragment {
    TextView nameUser;
    TextView followersUser;
    ImageView imageUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        nameUser = (TextView) view.findViewById(R.id.nameUser);
        followersUser = (TextView) view.findViewById(R.id.followersUser);
        imageUser = (ImageView) view.findViewById(R.id.imageUser);

        Bundle bundle = getArguments();
        if (bundle != null) {
            String name = bundle.getString("name");
            String friends = bundle.getString("friends");
            String profile = bundle.getString("profile");


            nameUser.setText(getString(R.string.Name) + name);
            followersUser.setText(getString(R.string.friends) + friends);
            Picasso.with(getActivity()).load(profile).into(imageUser);

        }


        return view;
    }


}
