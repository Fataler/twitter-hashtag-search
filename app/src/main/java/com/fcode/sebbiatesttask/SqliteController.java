package com.fcode.sebbiatesttask;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SqliteController extends SQLiteOpenHelper {
    private static final String LOGCAT = null;

    public SqliteController(Context applicationcontext) {
        super(applicationcontext, "androidsqlite.db", null, 1);
        Log.d(LOGCAT, "Created");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String query;
        query = "CREATE TABLE Tweets ( TweetId INTEGER PRIMARY KEY, TweetName TEXT,TweetDate TEXT,UserName TEXT, UserFriends TEXT,UserProfile TEXT)";
        database.execSQL(query);
        Log.d(LOGCAT, "Tweets Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query;
        query = "DROP TABLE IF EXISTS Tweets";
        database.execSQL(query);
        onCreate(database);
    }

    public void insertTweet(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
       // values.put("StudentName", queryValues.get("StudentName"));
        values.put("text", queryValues.get("text"));
        values.put("created_at", "created_at");
        //2nd screen
        values.put("profile_image_url", "profile_image_url");
        values.put("name", "name");
        values.put("friends_count", "friends_count");
        database.insert("Tweets", null, values);
        database.close();
    }

    public int updateTweet(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("text", queryValues.get("text"));
        values.put("created_at", "created_at");
        //2nd screen
        values.put("profile_image_url", "profile_image_url");
        values.put("name", "name");
        values.put("friends_count", "friends_count");
        return database.update("Tweets", values, "StudentId" + " = ?", new String[]{queryValues.get("StudentId")});

    }
}