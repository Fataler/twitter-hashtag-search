package com.fcode.sebbiatesttask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;


/**
 * Tweet fragment containing a tag search .
 */
public class MainActivityFragment extends Fragment {
    //JSON Node Names
    private static final String TAG_ARRAY = "statuses";
    private static final String TAG_USER = "user";
    private static final String TAG_TEXT = "text";
    private static final String TAG_CREATED = "created_at";
    private static final String TAG_FRIENDS = "friends_count";
    private static final String TAG_PROFILE = "profile_image_url";
    private static final String TAG_NAME = "name";


    ArrayList<HashMap<String, String>> tweetlist = new ArrayList<HashMap<String, String>>();
    private String hashtag;
    //SharedPreferences preferences;

    public MainActivityFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //SharedPreferences preferences = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        //hashtag=preferences.getString("pref","Ростов-на-Дону");
        //new Download().execute();
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Button tag = (Button) view.findViewById(R.id.search_tag);
        final EditText searchTweet = (EditText) view.findViewById(R.id.tweet_edit);
        tweetlist = new ArrayList<HashMap<String, String>>();


        tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchTweet.getText().toString().length() == 0) {
                    searchTweet.setError(getString(R.string.input_error_empty));
                } else {
                    hashtag = searchTweet.getText().toString().replace(" ", "");
                    tweetlist.clear();
                    new Download().execute();
                }
            }


        });
        return view;
    }


    private class Download extends AsyncTask<String, String, JSONArray> {

        private ProgressDialog pDialog;

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage(getString(R.string.loading_tweet));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... args) {
            try {
                HttpClient client = new DefaultHttpClient();
                String getURL = "https://api.twitter.com/1.1/search/tweets.json?q=" + hashtag;

                HttpGet get = new HttpGet(getURL);
                get.setHeader("Authorization", getString(R.string.token));
                HttpResponse responseGet = client.execute(get);
                HttpEntity resEntityGet = responseGet.getEntity();
                if (resEntityGet != null) {
                    //do something with the response
                    String retSrc = EntityUtils.toString(resEntityGet);
                    // parsing JSON
                    JSONObject result = new JSONObject(retSrc); //Convert String to JSON Object

                    JSONArray all = result.getJSONArray(TAG_ARRAY); //основной json
                    return all;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONArray all) {

            super.onPostExecute(all);
            pDialog.dismiss();
            try {
                // Getting JSON Array from URL

                for (int i = 0; i < all.length(); i++) {
                    JSONObject c = all.getJSONObject(i);
                    JSONObject user = c.getJSONObject(TAG_USER); //подобъект
                    // Storing  JSON item in a Variable
                    //1st
                    String text = c.getString(TAG_TEXT);
                    String time = c.getString(TAG_CREATED);
                    //2nd
                    String profile = user.getString("profile_image_url").replace("_normal", "");
                    String name = user.getString("name");
                    String friends = user.getString("friends_count");


                    // Adding value HashMap key => value

                    HashMap<String, String> map = new HashMap<String, String>();
                    //1st screen
                    map.put(TAG_TEXT, text);
                    map.put(TAG_CREATED, time);
                    //2nd screen
                    map.put(TAG_PROFILE, profile);
                    map.put(TAG_NAME, name);
                    map.put(TAG_FRIENDS, friends);

                    tweetlist.add(map);
                    //Log.i("GET ", name);
                    //Log.i("GET ", friends);
                     Log.i("GET ", profile);

                    ListView list = (ListView) getActivity().findViewById(R.id.listView);

                    ListAdapter adapter = new SimpleAdapter(getActivity(), tweetlist,
                            R.layout.list_item,
                            new String[]{TAG_TEXT, TAG_CREATED}, new int[]{
                            R.id.tweet, R.id.time});

                    list.setAdapter(adapter);


                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            // Toast.makeText(getActivity(), "You Clicked at " + position + tweetlist.get(position).get("friends_count")+ tweetlist.get(position).get("name")+ tweetlist.get(position).get("profile_image_url"), Toast.LENGTH_SHORT).show();
                            UserFragment uf = new UserFragment();
                            Bundle bundle = new Bundle();
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            bundle.putString("name", tweetlist.get(position).get("name"));
                            bundle.putString("profile", tweetlist.get(position).get("profile_image_url"));
                            bundle.putString("friends", tweetlist.get(position).get("friends_count"));
                            uf.setArguments(bundle);
                            ft.hide(MainActivityFragment.this);
                            ft.add(R.id.fragment, uf);
                            ft.addToBackStack(null);
                            ft.commit();

                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


}
